# odoo-dc-vscode



## Quickstart

- Right click on the containing folder.
- select "open with vscode" option.
- right click on the docker-compose.yaml file displayed at vscode's file explorer.
- select "Compose Up" option.

*In order to debug, just activate the "Odoo: Attach docker" option from Run & Debug*



