#!/bin/bash
set -e
/usr/bin/python3 -m pip install pip --upgrade
/usr/bin/python3 -m pip install debugpy

# Use this line to enable debugging
/usr/bin/python3 -m debugpy --listen 0.0.0.0:8888 /usr/bin/odoo -c /etc/odoo/odoo.conf

# Use this line to skip debugging
# /usr/bin/python3 /usr/bin/odoo -c /etc/odoo/odoo.conf


# Use this command to execute shell
# docker -it odoo_web odoo shell -c /etc/odoo/odoo_shell.conf -d DATABASE_NAME
# Use this command to execute module tests
# docker -it odoo_web odoo -i MODULE -c /etc/odoo/odoo_test.conf --test-enable -d DATABASE_NAME  --stop-after-init